<?php

use App\Http\Middleware\adminMiddleware;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controller('users', 'UsersController');
Route::controller('store', 'StoreController');
Route::controller('admin/categories', 'CategoriesController');
Route::controller('admin/products', 'ProductsController');
Route::get('/', array('uses'=> 'StoreController@getIndex'));