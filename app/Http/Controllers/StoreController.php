<?php namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\CartRowCollection;
use Gloudemans\Shoppingcart\CartCollection;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Input;
use App\Product;
use Illuminate\Http\Request;
use App\libs\Availability;
use Illuminate\Support\Facades\Redirect;


class StoreController extends Controller {

    public function __construct(){
        parent::__constuct();
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->middleware('auth', ['only' => ['postAddtocart', 'getCart', 'getRemoveitem']]);
	}

    public function getIndex(){
        return view('store.index')
            ->with('products', Product::take(4)->orderBy('created_at', 'DESC')->get());
    }

    public function getView($id){
        return view('store.view')
            ->with('product', Product::find($id));
    }

    public function getCategory($cat_id){
        return view('store.category')
            ->with('products', Product::where('category_id', '=', $cat_id)->paginate(6))
            ->with('category', Categories::find($cat_id));
    }

    public function getSearch(){
        $keyword = Input::get('keyword');

        return view('store.search')
            ->with('products', Product::where('title', 'LIKE', '%' . $keyword . '%')->get())
            ->with('keyword', $keyword);
    }

    public function postAddtocart(){
        $product = Product::find(Input::get('id'));
        $quantity = Input::get('quantity');

        Cart::add(array(
            'id'=>$product->id,
            'name'=>$product->title,
            'price'=>$product->price,
            'qty'=>$quantity,
            'image'=>$product->image

        ));

        return Redirect::to('store/cart');
    }

    public function getCart(){
        return view('store.cart')->with('products', Cart::content());
    }

    public function getRemoveitem($identifier){
        Cart::remove($identifier);
        return Redirect::to('store/cart');

    }

}
