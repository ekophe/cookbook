<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller {

    public function __construct(){
        parent::__constuct();
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function getNewaccount(){
        return view('users.newaccount');
    }

    public static function postCreate(){
        $validator = Validator::make(Input::all(), User::$rules);

        if($validator->passes()) {
            $user = new User;
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->telephone = Input::get('telephone');
            $user->save();

            return Redirect::to('users/signin')
                ->with('message', 'Thank you for creating a new account. Please sign in');
        }

        return Redirect::to('users/newaccount')
            ->with('message', 'Something went wrong')
            ->withErrors($validator)
            ->withInput();
    }

    public function getSignin(){
        return view('users.signin');
    }

    public function postSignin(){
        if(Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
            return Redirect::to('/')->with('message', 'Thanks for signing in');
        }

        return Redirect::to('users/signin')->with('message', 'Your email/password combo was incorrect');
    }

    public function getSignout(){
        Auth::logout();
        return Redirect::to('users/signin')->with('message', 'You have been signed out');
    }

}
