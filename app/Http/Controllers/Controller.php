<?php namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    public function __constuct(){
        $this->beforeFilter(function(){
            view()->share('catnav', Categories::all());
        });
    }

}
