<?php namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


class CategoriesController extends Controller
{

    public function __construct()
    {
        parent::__constuct();
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->middleware('admin');
    }

    public function getIndex()
    {
        return view('categories.index')->with('categories', Categories::all());
    }

    public function postCreate()
    {
        $validator = Validator::make(Input::all(), Categories::$rules);

        if ($validator->passes()) {
            $category = new Categories;
            $category->name = Input::get('name');
            $category->save();

            return Redirect::to('admin/categories/index')
                ->with('message', 'Category Created');
        }

        return Redirect::to('admin/categories/index')
            ->with('message', 'Something went wrong')
            ->withErrors($validator)
            ->withInput();

    }

    public function postDestroy() {
        $category = Categories::find(Input::get('id'));

        if($category) {
            $category->delete();
            return Redirect::to('admin/categories/index')
                ->with('message', 'Category deleted');
        }

        return Redirect::to('admin/categories/index')
            ->with('message', 'Something went wrong, please try again');
    }
}

