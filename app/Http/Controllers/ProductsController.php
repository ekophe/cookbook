<?php namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


class ProductsController extends Controller
{

    public function __construct()
    {
        parent::__constuct();
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->middleware('admin');
    }

    public function getIndex()
    {
        $categories = array();

        foreach(Categories::all() as $category) {
            $categories[$category->id] = $category->name;
        }

        return view('products.index')
            ->with('products', Product::all())
            ->with('categories', $categories);
    }

    public function postCreate()
    {
        $validator = Validator::make(Input::all(), Product::$rules);

        if ($validator->passes()) {
            $product = new Product;
            $product->category_id = Input::get('category_id');
            $product->title = Input::get('title');
            $product->description = Input::get('description');
            $product->price = Input::get('price');

            $image = Input::file('image');
            $filename = date('Y-n-d') . "-" . $image->getClientOriginalName();
            $path = public_path('img/products/' . $filename);
            Image::make($image->getRealPath())->resize(468, 291)->save($path);
            $product->image = 'img/products/'.$filename;
            $product->save();

            return Redirect::to('admin/products/index')
                ->with('message', 'Product Created');
        }

        return Redirect::to('admin/products/index')
            ->with('message', 'Something went wrong')
            ->withErrors($validator)
            ->withInput();

    }

    public function postDestroy() {
        $product = Product::find(Input::get('id'));

        if($product) {
            File::delete('public/'.$product->image);
            $product->delete();
            return Redirect::to('admin/products/index')
                ->with('message', 'Product deleted');
        }

        return Redirect::to('admin/products/index')
            ->with('message', 'Something went wrong, please try again');
    }

    public function postToggleAvailability(){
        $product = Product::find(Input::get('id'));
        $product->save();

        if($product) {
            $product->availability = Input::get('availability');
            $product->save();
            return Redirect::to('admin/products/index')->with('message', 'Product Updated');
        }

        return Redirect::to('admin/products/index')->with('message', 'Invalid Product');
    }
}

