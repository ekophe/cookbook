<?php namespace database\seeds;

use Illuminate\Database\Seeder;
use app\User;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder {
    public function run() {
        $user = new User;
        $user->firstname = 'Jonet';
        $user->lastname = 'Doe';
        $user->email = 'jonet@doe.com';
        $user->password = Hash::make('mypassword');
        $user->telephone = 55577771354;
        $user->admin = 1;
        $user->save();
    }
}